<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

get_header();
?>

<div id="primary" class="content-area">
  <main id="main-content" class="site-main">

    <?php if ( have_posts() ) : 
    $term = get_queried_object();
    $imageID = get_field('header_background_image', $term);
    $imageURL = wp_get_attachment_image_url( $imageID, 'full' )
      ?>

      <div
      style="<?= $imageURL ? "background-image: url({$imageURL}); background-position: center;" : null ?>" 
      class="bmcb-section container-fluid page-banner pseudo-black-overlay page-banner--slim">
        <div class="container">
          <div class="bmcb-row row">
            <div class="bmcb-column col col-12">
              <div class="bmcb-heading bmcb-module">
                <h1 class="bmcb-heading__title text-white">
                  <?php single_term_title() ?>
                </h1>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="bmcb-section container-fluid  pt-0 pb-0" style="background-color: #FFFFFF;">
        <div class="container">
          <div class="bmcb-row row py-2">
            <div id="" class="bmcb-column col col-12">
              <div class="bmcb-code bmcb-module text-dark">
                <?php echo apply_shortcodes('[navxt-crumbs]'); ?>
              </div>
            </div>
          </div>
        </div>
    </div>

    <div class="bmcb-section container-fluid bg-secondary">
      <div class="container">
        <div class="bmcb-row row">
          <div id="" class="bmcb-column col col-12">
            <div class="bmcb-code bmcb-module text-light">
              <?php 
                $term = get_queried_object()->slug;
                echo apply_shortcodes("[list_projects perpage='9' loadmore='true' type='{$term}']"); ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php endif;
		?>

  </main><!-- #main -->
</div><!-- #primary -->

<?php
if (function_exists('render_common_globals')) {
  echo render_common_globals();
}
get_footer();
