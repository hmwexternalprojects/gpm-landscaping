<?php
// Font awesome social icons for the footer
if (!function_exists('font_awesome_icon_shortcode')) {
  function font_awesome_icon_shortcode($atts) {   

    $atts = shortcode_atts( [
      "type" => 'fas',
      "class" => '',
      "icon" => ''
    ], $atts);
    
    ob_start();    
    
    if( $atts['icon'] ): ?>

<i class="<?= "{$atts['type']} fa-{$atts['icon']} {$atts['class']}"; ?>"></i>

<?php endif;
      
    return ob_get_clean();
  
  }
  add_shortcode('icon', 'font_awesome_icon_shortcode');
}


if (!function_exists('navxt_crumbs') && function_exists('bcn_display')) {
  function navxt_crumbs($atts) {   

    // $atts = shortcode_atts( [
    //   "type" => 'fas',
    //   "class" => '',
    //   "icon" => ''
    // ], $atts);
    
    ob_start();    

    echo '<div class="breadcrumbs">';

      bcn_display();

    echo '</div>';

    return ob_get_clean();
  
  }
  add_shortcode('navxt-crumbs', 'navxt_crumbs');
}
