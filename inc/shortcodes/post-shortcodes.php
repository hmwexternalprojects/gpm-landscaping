<?php

// [testi-slider] You can use any JS slider library you like. Default is Siema
if (!function_exists('testi_slider_shortcode')) {
  function testi_slider_shortcode($atts) {
    $atts = shortcode_atts( 
          array(
        'perpage' => 2,
        'image' => true,
        'width' => ''
          ), 
          $atts);
  
    $args = array(
          'post_type' => 'testimonial',
          'posts_per_page' => $atts['perpage'],
          'post_status' => 'publish'
      );
  
    $query = new WP_Query($args);
  
    ob_start();
    
    ?>

<div class="my-slider testimonial-slider">
  <?php if ($query->have_posts()) : ?>
    <div class="testimonial-slider__slides">
  <?php while($query->have_posts()) : $query->the_post(); 
          $ID = get_the_id();
          $width = $atts['width'];
          $imageEnabled = $atts['image'] !== 'false';
          $image = get_the_post_thumbnail_url($ID,'full'); // Returns URL of any custom size (obv)
          $name = get_the_title($ID);
          if (function_exists('get_field')) {
            $company = get_field('company', $ID);
          }
        ?>

          <div
          class="testimonial-slider__slide <?php echo ($width) ? "is-$width" : '' ?> <?php echo !$imageEnabled ? 'image-false' : 'image-true'; ?>"
          <?php echo $imageEnabled ? "style=\"background: url('$image')\"" : ''; ?>>
          <div class="testimonial-slider__content-box">
            <div class="testimonial-slider__content">
              <div class="testimonial-slider__body"><?php the_content(); ?></div>
              <div class="testimonial-slider__info"><?php echo sprintf("<span class='d-block  text-primary justify-center mt-2'><strong>%s</strong> %s</span>", $company, $name); ?></div>
            </div>        
          </div>
        </div>
        <?php endwhile; ?>
      </div>
        <div class="testimonial-slider__navigation slider__navigation">
          <div class="testimonial-slider__arrow-group slider__arrow-group">
            <div class="testimonial-slider__arrow slider__arrow arrow-left">
              <i class="fa fa-chevron-left"></i>
            </div>
            <div class="testimonial-slider__arrow slider__arrow arrow-right">
              <i class="fa fa-chevron-right"></i>
            </div>
          </div>
        </div>
        <?php endif; ?>
      </div>

<?php
    $output = ob_get_clean();
      wp_reset_postdata();
      return $output;
  }
  add_shortcode('testi-slider', 'testi_slider_shortcode');
}

// [list-posts] This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
if (!function_exists('list_posts_shortcode')) {
  function list_posts_shortcode($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts( [
		"perpage" => 6,
		"offset" => 0,
		"cols" => 3,
		"cat" => null,
		"post_type" => 'post',
		"paged" => true,
	], $atts );
	
	if (!get_query_var('categories') && isset($atts['cat'])) {
		$categoryIDs = $atts['cat']; 
	}

	$pagination = ($atts['perpage'] > 6 || $atts['perpage'] === -1);	
	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );

	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	
	$args = [
		"posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => $atts['post_type'],
		"post_status" => "publish",
		"category__in" => $categoryIDs ? [$categoryIDs] : ''
	];

	if ($pagination) {
		$args['paged'] = $current_page;
	}
	
	$query = new WP_Query($args); 

	$total_rows = max( 0, $query->found_posts - $offset_start );
	$total_pages = ceil( $total_rows / $atts['perpage'] );

	ob_start();
	if ($query->have_posts()) : ?>

<div class="article-grid grid grid-1 grid-lg-<?php echo $atts['cols'];?> gap-3">
  <?php while ($query->have_posts()) : $query->the_post(); ?>

  <?php get_template_part('template-parts/content', 'post'); ?>

  <?php endwhile; ?>
</div>

<?php if ($pagination) : ?>
<div class="pagination">
  <?php echo paginate_links( array(
					'total'   => $total_pages,
					'current' => $current_page,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
				) ); ?>
</div>
<?php endif; 
		// pagination($total_pages) 
		?>

<?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
}
  add_shortcode('list-posts', 'list_posts_shortcode');
}

// [list_projects] This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
if (!function_exists('list_projects_shortcode')) {
  function list_projects_shortcode($atts, $request) {
	
	if (get_query_var('categories')) {
		$categoryIDs = get_query_var('categories');
	}

	$atts = shortcode_atts( [
		"perpage" => 3,
		"offset" => 0,
    "cols" => 3,
    "title" => null,
    "loadmore" => 'false',
		"type" => null,
		"cat_filter" => false,
		"post_type" => 'project',
    "paged" => true,
    "excerpt" => true,
	], $atts );
	
	if (!get_query_var('categories') && isset($atts['cat'])) {
		$categoryIDs = $atts['cat']; 
	}

	$current_page = get_query_var('paged');
	$current_page = max( 1, $current_page );

	$offset_start = $atts['offset'];
	$offset = ( $current_page - 1 ) * $atts['perpage'] + $offset_start;
	$taxonomy = 'project_type';

	$args = [
		"posts_per_page" => $atts['perpage'],
		"offset" => $offset,
		"post_type" => $atts['post_type'],
		"post_status" => "publish",
		"category__in" => $categoryIDs ? [$categoryIDs] : ''
  ];
  
  if ($atts['type']) :
    $args["tax_query"] = array(
      array(
        'taxonomy' => $taxonomy,
        'field'    => 'slug',
        'terms'    => array($atts['type'])
        )
      );
  endif;
  
	
  $pagination = ($atts['perpage'] >= 6 || $atts['perpage'] === -1);	
  
  if ($pagination) {
    $args['paged'] = $current_page;
  }
  
	$query = new WP_Query($args); 

	$total_rows = max( 0, $query->found_posts - $offset_start );
  $total_pages = ceil( $total_rows / $atts['perpage'] );

	ob_start();
	if ($query->have_posts()) : ?>

<div class="article-grid-wrapper">
<div class="article-grid__title-bar ">
  <?php if (isset($atts['title'])) : ?>
    <h3 class="article-grid__title text-light header-underline">Projects</h3>
  <?php endif; ?>
  <?php 
  if ($atts['cat_filter'] === 'true') : 
    $terms = get_terms( array(
      'taxonomy' => $taxonomy,
      'hide_empty' => false,
    ));
    ?>
  <ul 
    data-queryvars='<?php echo json_encode($query->query_vars); ?>'
    data-taxonomy='<?php echo $taxonomy ?>'
    data-templatepart="project"
    data-container=".article-grid"
    id="terms-list" class="terms-list flex -mx-2">
      <li class="terms-list__term is-active">All</li>
    <?php foreach($terms as $term) : ?>
      <li data-term="<?= $term->slug ?>" class="terms-list__term"><?= $term->name ?></li>
    <?php endforeach; ?>
  </ul>
    <?php endif; ?>
    </div>  

  <div data-staggerin="{delay: 100, y: 0}" class="article-grid md:cols-<?= $atts['cols'] ?> gap-3">
    <?php while ($query->have_posts()) : $query->the_post(); ?>

      <?php include(locate_template( 'template-parts/content-project.php' )); ?>

    <?php endwhile; ?>
  </div>
</div>

<?php if ($pagination && $total_pages > 1) : ?>
  <?php if ($atts['loadmore'] === 'true') : ?>
    <div class="loadmore-button mt-4 btn__loader"
      data-queryvars='<?php echo json_encode($query->query_vars); ?>'
      data-currentpage="<?php echo $current_page; ?>" 
      data-totalpages="<?php echo $total_pages; ?>"
      data-perpage="<?php echo $atts['perpage']; ?>"
      data-templatepart="project"
      data-container=".article-grid">
      Load more
    </div>
  <?php else : ?>
    <div class="pagination">
      <?php echo paginate_links( array(
        'total'   => $total_pages,
        'current' => $current_page,
        'prev_text' => 'Prev',
        'next_text' => 'Next'
        ) ); ?>
    </div>
  <?php endif; ?>
<?php endif; 
		// pagination($total_pages) 
		?>

<?php 
	endif;
	wp_reset_postdata();
	return ob_get_clean();
}
  add_shortcode('list_projects', 'list_projects_shortcode');
}

// Echo either Yogi Bear or Boo Boo, Yogi: [echo-field field="content"] | Boo Boo: [echo-field acf_field="anything"]
// More Yogi [echo-field field="post_thumbnail" size="thumbnail"] // params are useful for image sizes
if (!function_exists('echo_field_shortcode')) {
  function echo_field_shortcode($atts) {
      if (is_admin()) {return null;}
  
      extract(shortcode_atts(array(
          'field' => '',
          'acf_field' => '',
          'params' => ''
      ), $atts));
  
      $ID = get_the_ID();
  
      $output = false;
  
      if (!empty($acf_field)) {
          // Check to ensure boo boo is available
          if (function_exists('get_field')) {
              $output = get_field($acf_field);
          }
      }
  
      // Yogi takes presidence -- make sure field is not set if acf_field is
      if (!empty($field)) {
          $output = 'get_the_' . $field; 
      } 
  
      // There are inconsistencies with the amount of params in each wordpress function so here is where you adjust 
      // as needed on a case by case... E.G ID needs to come first when doing custom image sizes, but not others
      if ($output && function_exists($output)) {
          switch($output) {
              case 'get_the_post_thumbnail':
                  $output = $output($ID, $params);
              break;
              case 'get_the_ID':
                  $output = $ID;
              break; 
              default:
                  $output = $output();
          }
      }
  
      return $output ?: 'There were no fields matching your query';
  }
  add_shortcode('echo-field', 'echo_field_shortcode');
}


// [related-posts cat="" offset="" post_type=""] This is a universal shortcode to display any post/posttype
// In a css grid format with pagination and a basic category filter
if (!function_exists('related_posts_shortcode')) {
  function related_posts_shortcode($atts, $request) {
    
    if (get_query_var('categories')) {
      $categoryIDs = get_query_var('categories');
    }
  
    $atts = shortcode_atts( [
      "perpage" => 6,
      "offset" => 0,
      "cols" => 3,
      "gap" => 0,
      "cat" => null,
      "post_type" => 'post',
      "paged" => true,
      "text" => 'light'
    ], $atts );
    
    if (!get_query_var('categories') && isset($atts['cat'])) {
      $categoryIDs = $atts['cat']; 
    }
    
    $args = [
      "posts_per_page" => $atts['perpage'],
      "post_type" => $atts['post_type'],
      "post_status" => "publish",
      "post__not_in" => array(get_the_ID()),
      "category__in" => $categoryIDs ? [$categoryIDs] : ''
    ];
    
    // Headers white or black
    $color = $atts['text'] === "light" ? 'white' : 'black';
    $opposite_color = $atts['text'] !== "light" ? 'black' : 'white';
  
    
    $query = new WP_Query($args); 
  
    ob_start();
    if ($query->have_posts()) : ?>


<div class="section container">
  <div class="article-grid article-grid__related-posts grid">
    <?php while ($query->have_posts()) :       
          $query->the_post();
          $index = $query->current_post; 
          ?>

    <div class="col-<?php echo 12 / intval($atts['cols']);?>">
      <?php include(locate_template("template-parts/article-condensed.php")); ?>
    </div>

    <?php endwhile; ?>

  </div>
</div>

<?php 
    endif;
    wp_reset_postdata();
    return ob_get_clean();
  }
  add_shortcode('related-posts', 'related_posts_shortcode');
}
