<?php 

function hmw_filter_posts($request) {
  // Get the query directly
  $args = json_decode( stripslashes( $request['query'] ), true );
  $offset = $request['offset'];
  $taxonomy = $request['taxonomy'];
  $category = $request['category'];

  // we need next page to be loaded
  $args['post_status'] = 'publish';
  $args['offset'] = $offset;

  if (isset($taxonomy) && isset($category)) :
    $args["tax_query"] = array(
      array(
        'taxonomy' => $taxonomy,
        'field'    => 'slug',
        'terms'    => array($category)
      )
    );
  endif;

  // Template Part For Loading Content
  $template_part = esc_attr($request['template_part']);

  // return new WP_REST_Response( $args, 200 );

  // return new WP_REST_Response( $args, 200 );
  // it is always better to use WP_Query but not here
  $q = new WP_query( $args );

  ob_start();
  if( $q->have_posts() ) :

    $newPosts = [];

    while( $q->have_posts() ): $q->the_post();


      // Render new posts with the correct template, e.g content-post-grid, content-single-product, etc
      get_template_part( 'template-parts/content-' . $template_part );

      // if we get them back correctly, return the successful rest response and status code
      
    endwhile;

    // ob_end_clean();

    $total_rows = max( 0, $q->found_posts - $offset);
    $total_pages = ceil( $total_rows / $args['posts_per_page'] );
  
      $res = [
        'query_vars' => json_encode($args),
        'total_pages' => $total_pages,
        'posts' => json_encode(ob_get_clean(), JSON_HEX_QUOT | JSON_HEX_TAG),
      ];

      wp_reset_postdata();

      return new WP_REST_Response( $res, 200 );
      
  else: 
	// Otherwise return an error
    return new WP_Error( 'no-posts', __( 'There were no more posts found', 'hmw' ));
  endif;

  wp_reset_postdata();

}

// Add rest API endpoint to handle this
add_action( 'rest_api_init', function () {
	// For simplicity, sticking to wp/v2 (front-end already has access to this as a global variable)
	register_rest_route( 'wp/v2', '/hmw_filter_posts', array(
	'methods' => 'POST',
	'callback' => 'hmw_filter_posts',
	) );
});
