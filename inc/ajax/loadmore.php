<?php 
function hmw_ajax_load_more($request) {
  // Get the query directly
  $args = json_decode( stripslashes( $request['query'] ), true );
  // we need next page to be loaded
  $args['paged'] = $request['page'] + 1; 
  $args['post_status'] = 'publish';

  $args['offset'] = ($args['posts_per_page'] * $request['current_page']);
  
  // Template Part For Loading Content
  $template_part = esc_attr($request['template_part']);

  // return new WP_REST_Response( $args, 200 );

  // return new WP_REST_Response( $args, 200 );
  // it is always better to use WP_Query but not here
  query_posts( $args );

  if( have_posts() ) :

    while( have_posts() ): the_post();

      // Render new posts with the correct template, e.g content-post-grid, content-single-product, etc
      $newPosts = get_template_part( 'template-parts/content-' . $template_part );

      // if we get them back correctly, return the successful rest response and status code
      if ( is_array( $newPosts ) ) {
        return new WP_REST_Response( json_encode($newPosts), 200 );
      }

    endwhile;
  else: 
	// Otherwise return an error
    return new WP_Error( 'no-posts', __( 'There were no more posts found', 'hmw' ));
  endif;

}

// Add rest API endpoint to handle this
add_action( 'rest_api_init', function () {
	// For simplicity, sticking to wp/v2 (front-end already has access to this as a global variable)
	register_rest_route( 'wp/v2', '/hmw_ajax_load_more', array(
	'methods' => 'POST',
	'callback' => 'hmw_ajax_load_more',
	) );
});
