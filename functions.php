<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

function hmw_remove_parent_stuff() {
    wp_dequeue_style( 'hmw-frontend-styles' );
    wp_deregister_style( 'hmw-frontend-styles' );
    wp_dequeue_script( 'hmw-frontend-scripts' );
    wp_deregister_script( 'hmw-frontend-scripts' );
}

add_action( 'wp_enqueue_scripts', 'hmw_remove_parent_stuff', 20 );

function theme_enqueue_styles() {
  // Enqueue Fonts (if any)
  options_google_font_enqueue();

  // Get the theme data
	// $the_theme = wp_get_theme();
  wp_register_script( 'hmw-child-frontend-scripts', get_stylesheet_directory_uri() . '/public/frontend-bundle.js', array('jquery'), null, true );
	wp_localize_script( 'hmw-child-frontend-scripts', 'global_vars', array(
    'apiBase' => get_rest_url(get_current_blog_id(), 'wp/v2'),
		'admin_ajax_url' => admin_url( 'admin-ajax.php' )
  ) );
  
	// Loads bundled frontend CSS.
  wp_enqueue_style( 'hmw-child-frontend-styles', get_stylesheet_directory_uri() . '/public/frontend.css' );
	wp_enqueue_style( 'hmw-theme-options', get_stylesheet_directory_uri() . '/public/hmw-theme-options.css' );	
  	
  wp_enqueue_script('hmw-child-frontend-scripts');
  
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

// Any JS that needs to run immediate after the body tag opens (e.g to remove the no-js flag on the body)
add_action('wp_body_open', function() {
  $path = get_stylesheet_directory_uri() . '/src/js/init.js';
  echo "<script src='{$path}'></script>";
});


function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'understrap-child', get_stylesheet_directory() . '/languages' );
}

add_action( 'after_setup_theme', 'add_child_theme_textdomain' );


function add_custom_image_sizes() {
  add_image_size( 'page-banner-slim', 3840, 430, true );
}

add_action( 'after_setup_theme', 'add_custom_image_sizes' );



/****************************************************************************************************************
* If any of these files don't exist it will revert to parent themes version
*/
include get_theme_file_path( '/inc/shortcodes/default-shortcodes.php' ); // These are the default shortcodes we bundle 
include get_theme_file_path( '/inc/shortcodes/post-shortcodes.php' ); // Everything related to posts
include get_theme_file_path( '/inc/shortcodes/page-shortcodes.php' ); // Everything related to pages
include get_theme_file_path( '/inc/shortcodes/global.php' ); // Everything related to pages


/****************************************************************************************************************
* Include Custom REST API Endpoints
*/
include get_theme_file_path('/inc/ajax/loadmore.php');
include get_theme_file_path('/inc/ajax/filter-projects.php');

/**
 * Get all the registered image sizes along with their dimensions
 *
 * @global array $_wp_additional_image_sizes
 *
 * @link http://core.trac.wordpress.org/ticket/18947 Reference ticket
 * @return array $image_sizes The image sizes
 */
function _get_all_image_sizes() {
	global $_wp_additional_image_sizes;

	$default_image_sizes = array( 'thumbnail', 'medium', 'large' );
	 
	foreach ( $default_image_sizes as $size ) {
		$image_sizes[$size]['width']	= intval( get_option( "{$size}_size_w") );
		$image_sizes[$size]['height'] = intval( get_option( "{$size}_size_h") );
		$image_sizes[$size]['crop']	= get_option( "{$size}_crop" ) ? get_option( "{$size}_crop" ) : false;
	}
	
	if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) )
		$image_sizes = array_merge( $image_sizes, $_wp_additional_image_sizes );
		
	return $image_sizes;
}

if (!function_exists('render_common_globals')) {
  function render_common_globals() {
    global $buildy;
    ob_start();
      echo $buildy->renderFrontend('121'); 
      echo $buildy->renderFrontend('132'); 
      echo $buildy->renderFrontend('157'); 
    return ob_get_clean();
  }
}



function disable_message_load_field( $field ) {
$field['readonly'] = 1;
  return $field;
}
add_filter('acf/load_field/name=sub_field_1', 'disable_message_load_field');
add_filter('acf/load_field/name=sub_field_2', 'disable_message_load_field');
