<?php 
  $ID = get_the_ID();
	$index = $query->current_post; 
  $postURL = get_permalink();
?>

<div class="article-grid__article article-grid__article--project">
    <?php
			$imageSize = 'article-grid'; 
			$image = get_the_post_thumbnail($ID, $imageSize, ['class' => 'rounded']);
      echo sprintf("<a class='article-grid__image-wrapper' href='%s'>%s</a>", $postURL, $image); 
    ?>
    <!-- <span class="article-grid__date"><?php the_time('F jS, Y'); ?></span> -->
    <h3 class="article-grid__title">
      <a href="<?php echo $postURL; ?>">
        <?php the_title(); ?>
      </a>
    </h3>
    <?php if ($atts['excerpt'] !== 'false') : ?>
    <div class="article-grid__excerpt">
      <?php echo wp_trim_words(get_the_excerpt(), 25); ?>
    </div>
    <?php endif; ?>
    <a href="<?php echo $postURL; ?>" class="article-grid__readmore self-start btn--has-arrow btn--has-underline d-inline-flex">
      Read more
    </a>
  </div>
