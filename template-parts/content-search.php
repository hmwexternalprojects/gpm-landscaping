<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package _s
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('search-results__item'); ?>>
  <?php if (get_the_post_thumbnail()) : ?>
  <div class="left">
    <?php hmw_post_thumbnail('thumbnail'); ?>
  </div>
  <?php endif; ?>
  <div class="right">
    <header class="entry-header flex items-center">
      <div class="search-results__post-type mr-2">
        <?php echo get_post_type( get_the_ID() ) . ':'; ?>
      </div>
      <?php the_title( sprintf( '<h2 class="entry-title mb-0"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

      <?php if ( 'post' === get_post_type() ) : ?>
      <div class="entry-meta">
        <?php
            hmw_posted_on();
            hmw_posted_by();
            ?>
      </div><!-- .entry-meta -->
      <?php endif; ?>
    </header><!-- .entry-header -->

    <div class="entry-summary">
      <?php the_content(); ?>
    </div><!-- .entry-summary -->

    <footer class="entry-footer">
      <?php hmw_entry_footer(); ?>
    </footer><!-- .entry-footer -->
  </div>
</article><!-- #post-<?php the_ID(); ?> -->
