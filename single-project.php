<?php
 get_header();
?>

<div <?php post_class(); ?> id="main-content">

	<?php while ( have_posts() ) : the_post(); ?>
  
  <?php 
    $ID = get_the_ID();
    $slider = get_field('slider', $ID);
    $slider_acf = get_field('slider_acf', $ID);
    ?>
		<div class="content-wrap project-wrap">
      <div class="project-header">
        <div class="project-slider-wrapper position-relative">
          <div class="project-slider position-relative z-low">
            <?php if (isset($slider_acf)) : foreach($slider_acf as $slide) : ?>
            <div class="project-slider__slide-wrapper">
              <?php echo wp_get_attachment_image( $slide, 'full', '', ["class" => "project-slider__image"] ); ?>
              <!-- <div class="project-slider__image-text">
                <?= wp_get_attachment_caption($slide); ?>
              </div> -->
            </div>            
            <?php endforeach; endif; ?>
          </div>
          <div class="project-slider__navigation slider__navigation">
            <div class="project-slider__arrow-group slider__arrow-group">
              <div class="project-slider__arrow slider__arrow arrow-left">
                <i class="fa fa-chevron-left"></i>
              </div>
              <div class="project-slider__arrow slider__arrow arrow-right">
                <i class="fa fa-chevron-right"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="bmcb-section py-0">
          <div class="container">
            <div class="bmcb-row py-2">
              <div class="bmcb-column col">
                <?= apply_shortcodes('[navxt-crumbs]') ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="bmcb-section pt-0">
        <div class="container">
          <div class="bmcb-row py-0">
            <h1 class="project-title text-uppercase"><?= get_the_title($ID); ?></h1>
          </div>
          <div class="bmcb-row pt-0">
            <div class="bmcb-column col">
              <div class="project-content">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
          <div class="bmcb-row pt-0">
            <div class="bmcb-column col">
              <div class="project-navigation flex flex-wrap justify-center">
              <?php 
              	$args = [
                  "posts_per_page" => -1,
                  "post_type" => 'project',
                  "post_status" => "publish",
                ];
                
                $posts = get_posts($args);
                $current_post = array_search ( get_the_ID(), array_column($posts, 'ID') );

              ?>
                  <?php echo $posts[$current_post + 1] ? sprintf( "<a class='btn mx-1 mb-2 md:mb-0 w-full text-center md:w-auto whitespace-nowrap' href='%s'>%s</a>", get_permalink($posts[$current_post + 1]), 'Previous Project' ) : null; ?>
                  <?php echo $current_post >= 1 ? sprintf( "<a class='btn mx-1 w-full md:w-auto text-center whitespace-nowrap' href='%s'>%s</a>", get_permalink( $posts[$current_post - 1] ), 'Next Project' ) : null; ?>
                  <?php next_posts_link( 'Next Project' ); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <?php 
      if (function_exists('get_field') && get_field('display_globals', $id)) {
        global $buildy;
        $globals = get_field('display_globals', $id);
        foreach($globals as $global) {
          // echo $global;
          echo $buildy->renderFrontend($global); 
        }
      } 
    ?>

	<?php endwhile; ?>
</div> <!-- #main-content -->

<?php

get_footer();
