export const isElementVisible = (el) => {
  var rect = el.getBoundingClientRect();

  return rect.bottom > 0 &&
    rect.right > 0 &&
    rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */ &&
    rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */;
}

/*!
 * Get all of an element's parent elements up the DOM tree until a matching parent is found
 * (c) 2019 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Node}   elem     The element
 * @param  {String} parent   The selector for the parent to stop at
 * @param  {String} filter   The selector to filter against [optional]
 * @return {Array}           The parent elements
 */
export const getParentsUntil = (elem, parent, filter) => {

  // Setup parents array
  let parents = [];

  // Get matching parent elements
  while (elem && elem !== document) {

    // If there's a parent and the element matches, break
    if (parent) {
      if (elem.matches(parent)) break;
    }

    // If there's a filter and the element matches, push it to the array
    if (filter) {
      if (elem.matches(filter)) {
        parents.push(elem);
      }
      continue;
    }

    // Otherwise, just add it to the array
    parents.push(elem);

    elem = elem.parentNode;

  }

  return parents;

};

export const onClickOrTap = (elem, callback, method = 'add') => {

  // Make sure a callback is provided
  if (!callback || typeof (callback) !== 'function') return;

  // Variables
  var isTouch, startX, startY, distX, distY;

  /**
   * touchstart handler
   * @param  {event} event The touchstart event
   */
  var onTouchStartEvent = function (event) {
    // Disable click event
    isTouch = true;

    // Get the starting location and time when finger first touches surface
    startX = event.changedTouches[0].pageX;
    startY = event.changedTouches[0].pageY;
  };

  /**
   * touchend handler
   * @param  {event} event The touchend event
   */
  var onTouchEndEvent = function (event) {

    // Get the distance travelled and how long it took
    distX = event.changedTouches[0].pageX - startX;
    distY = event.changedTouches[0].pageY - startY;

    // If a swipe happened, do nothing
    if (Math.abs(distX) >= 7 || Math.abs(distY) >= 10) return;

    // Run callback
    callback(event);

  };

  /**
   * click handler
   * @param  {event} event The click event
   */
  var onClickEvent = function (event) {
    // If touch is active, reset and bail
    if (isTouch) {
      isTouch = false;
      return;
    }

    // Run our callback
    callback(event);
  };

  // Event listeners
  elem[`${method}EventListener`]('touchstart', onTouchStartEvent, false);
  elem[`${method}EventListener`]('touchend', onTouchEndEvent, false);
  elem[`${method}EventListener`]('click', onClickEvent, false);

};
