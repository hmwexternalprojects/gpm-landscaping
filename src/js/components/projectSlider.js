import Siema from 'siema';

let sliderClass = '.project-slider'
let projectSlider = e => e

if (document.querySelector(sliderClass)) {


  projectSlider = () => {
    let currentIndex;

    const prev = document.querySelectorAll('.arrow-left');
    const next = document.querySelectorAll('.arrow-right');

    const slider = new Siema({
      selector: sliderClass,
      duration: 200,
      easing: 'ease-out',
      perPage: 1,
      startIndex: 0,
      draggable: true,
      multipleDrag: true,
      threshold: 20,
      loop: true,
      rtl: false,
      onInit: function () {
        currentIndex = this.currentSlide
      },
      onChange: function () {

        let pagination = this.selector.parentNode.querySelector('.project-slider__navigation-dots');
        let isActive = pagination.querySelector('.is-active');

        // Remove isActive from any other ones
        if (isActive) {
          isActive.classList.remove('is-active')
        }

        // Make the clicked one active
        pagination.children[this.currentSlide].classList.add('is-active');

      },
    });
    slider.addPagination();

    if (next && prev) {
      prev.forEach((el) => el.addEventListener('click', () => slider.prev()))
      next.forEach((el) => el.addEventListener('click', () => slider.next()))
    }
  }

}

Siema.prototype.addPagination = function () {
  const ul = document.createElement('ul');
  ul.classList.add('project-slider__navigation-dots', 'container');
  for (let i = 0; i < this.innerElements.length; i++) {
    const li = document.createElement('li');
    // li.textContent = i;

    if (this.currentSlide === i) {
      li.classList.add('is-active')
    }

    li.addEventListener('click', (el) => {
      this.goTo(i)
    });

    ul.appendChild(li)
  }
  this.selector.parentNode.appendChild(ul);
}

export { projectSlider }
