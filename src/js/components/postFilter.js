import { enableStaggerIn } from "../globalAnimations";

export default function () {
  let termList = document.getElementById('terms-list')
  if (termList) {
    let queryVars = termList.getAttribute('data-queryvars') || null,
      taxonomy = termList.getAttribute('data-taxonomy') || null,
      currentPage = 0,
      offset = 0,
      template_part = termList.getAttribute('data-templatepart') || 'post',
      targetContainer = termList.getAttribute('data-container') || '.article-grid',
      targetContainerEL = document.querySelector(targetContainer)

    termList.style.opacity = 1;

    termList.addEventListener('click', function (e) {
      e.preventDefault()
      e.stopPropagation()

      let target = e.target;

      if (target.classList.contains('is-active') || !target.classList.contains('terms-list__term')) {
        return;
      }


      let category = e.target.getAttribute('data-term') || null

      let data = {
        query: queryVars,
        page: currentPage,
        offset,
        taxonomy,
        category,
        template_part
      };

      if (!global_vars && global_vars.apiBase) { throw new Error('global vars and API base need to be set in the main scripts enqueue') }

      targetContainerEL.classList.add('is-loading')

      fetch(`${global_vars.apiBase}/hmw_filter_posts`, {
        method: "post",
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
      })
        .then(res => res.json())
        .then(data => {
          // Dump all the new posts in
          targetContainerEL.innerHTML = JSON.parse(data.posts)

          enableStaggerIn();

          let payload = { detail: { query_vars: data.query_vars, total_pages: data.total_pages } }

          target.parentNode.querySelector('.is-active').classList.remove('is-active');
          target.classList.add('is-active');

          targetContainerEL.classList.remove('is-loading')

          let headerHeight = document.getElementById('main-header').clientHeight

          let parent = target.closest('.bmcb-section')

          // parent.scrollIntoView({
          //   behavior: "smooth"
          // })

          window.scrollTo({
            top: parent.offsetTop - headerHeight,
            behavior: "smooth"
          });

          // Dispatch an event with the new query information that things like loadmore can hook into
          window.dispatchEvent(new CustomEvent("query_change", payload));

        }).catch(err => {
          console.log(err)
        })
    });
  }
}
