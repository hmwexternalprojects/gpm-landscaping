import { enableStaggerIn } from "../globalAnimations";

export default function () {
  /*  
    All these required variables can be dropped into your custom loop
    $query is whatever variable you assigned to the query like: $variable = new WP_Query()
      $queryVars = json_encode($query->query_vars, true);
      $current_page = get_query_var('paged');
      $current_page = max( 1, $current_page );
      $total_pages = ceil( $total_rows / $atts['perpage'] );
      
    And here is the code/DIV you would use to show the actual button required a shortcode att of "pagination" === "loadmore"
      <?php if ($atts['pagination'] === 'loadmore') : ?>
        <div class="loadmore-button btn__loader"
          data-queryvars='<?php echo json_encode($query->query_vars); ?>'
          data-currentpage="<?php echo $current_page; ?>"
          data-totalpages="<?php echo $total_pages; ?>"
          data-perpage="<?php echo $atts['perpage']; ?>"
          data-templatepart="project"
          data-container=".article-grid">
          Load more
        </div>
      <?php endif; ?>

    So if you're using the JS routes like JP does in the starter project, this can go only on the route it's loaded
    e.g "page" or "news-page". If you just want it available everywhere, add it to common.js
    
    import loadMore from './path/to/this-file'
    
    finalize() {
      // call it inside finalize
      loadMore()
    }
  */
  let loadMoreBtn = document.querySelector('.loadmore-button')

  if (loadMoreBtn) {
    let queryVars = loadMoreBtn.getAttribute('data-queryvars'),
      currentPage = loadMoreBtn.getAttribute('data-currentpage'),
      offset = loadMoreBtn.getAttribute('data-offset') || null,
      totalPages = loadMoreBtn.getAttribute('data-totalpages'),
      template_part = loadMoreBtn.getAttribute('data-templatepart'),
      targetContainer = loadMoreBtn.getAttribute('data-container'),
      original_text = loadMoreBtn.innerHTML

    // Listen for changes in the query (e.g via a filter)
    window.addEventListener('query_change', function (e) {
      queryVars = e.detail.query_vars;
      currentPage = 1
      totalPages = e.detail.total_pages
      loadMoreBtn.classList.remove('is-loading')
      if (currentPage === totalPages) {
        disable_button()
      } else {
        enable_button()
      }
    })

    loadMoreBtn.addEventListener('click', function (e) {
      e.preventDefault()
      loadMoreBtn.classList.add('is-loading')

      let data = {
        query: queryVars,
        current_page: currentPage,
        offset,
        template_part
      };

      if (currentPage < totalPages) {
        fetch(`${global_vars.apiBase}/hmw_ajax_load_more`, {
          method: "post",
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(data)
        })
          .then(res => res.text())
          .then(data => {

            loadMoreBtn.setAttribute('data-currentpage', currentPage++)

            // return console.log(JSON.parse(data))
            document.querySelector(targetContainer).innerHTML += data

            enableStaggerIn();

            loadMoreBtn.classList.remove('is-loading')


            if (currentPage >= totalPages) {
              disable_button()
            }

            currentPage++

          }).catch(err => {
            disable_button()
            console.log(err)
          })
      }

      if (currentPage === totalPages) {
        disable_button()
      }

    });

    function enable_button() {
      loadMoreBtn.classList.remove('disabled');
      loadMoreBtn.innerHTML = original_text;
    }

    function disable_button() {
      loadMoreBtn.classList.add('disabled');
      loadMoreBtn.innerHTML = 'No more results'
    }
  }
}
