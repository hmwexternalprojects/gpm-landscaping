import debounce from '../util/debounce';
import { onClickOrTap } from './../util/elements';

export default function (options = {}) {
  let windowWidth = window.innerWidth,
    mainNav = document.getElementById('main-nav'),
    body = document.body,
    mobileMenuActive = false,
    originalHrefs = [],
    mobileToggle = document.getElementById('menu-toggle'),
    mainNavParent,
    mobileMenu;

  options = {
    always_show: false,
    ...options
  }

  // Hide toggle 
  if (!options.always_show) {
    mobileToggle.style.display = 'none';
  }

  if (!mainNav) {
    console.log('For responsive menu to work, your main nav needs the ID: %c#main-nav', 'color: red')
    return
  }

  if (windowWidth < 980 && !mobileMenuActive || options.always_show) {
    setupMobileMenu()
  }

  window.addEventListener('resize', debounce(() => {
    windowWidth = window.innerWidth;
    if (windowWidth < 980) {
      if (!mobileMenuActive) {
        setupMobileMenu()
      }
    } else {
      if (mobileMenuActive && !options.always_show) {
        destroyMobileMenu()
      }
    }
  }))

  function setupMobileMenu() {
    mobileMenuActive = true;

    // Show toggle 
    if (!options.always_show) {
      mobileToggle.style.display = 'block';
    }

    body.classList.add('has-mobile-menu')

    if (!options.always_show) {
      mobileMenu = document.createElement('div');
      mobileMenu.classList.add('mobile-menu');
      body.append(mobileMenu)

      // Remember original positions
      mainNavParent = mainNav.parentNode

      let mobileContainer = document.querySelector('.mobile-menu')

      // Move to mobile nav
      mobileContainer.append(mainNav)

      console.log(mobileContainer)
    } else {
      mobileMenu = document.querySelector('.mobile-menu');
    }

    onClickOrTap(mobileToggle, toggleVisibility)

    setup_collapsible_submenus();
  }

  function closeOnOutsideClick(e) {
    let target = e.target;
    if (!mobileMenu.contains(target)) {
      toggleVisibility(e)
    }
  }

  function toggleVisibility(e) {
    e.preventDefault();
    e.stopPropagation();
    if (body.classList.contains('mobile-menu-is-visible')) {
      body.classList.remove('mobile-menu-is-visible');
      mobileMenu.setAttribute('aria-expanded', 'false');
      document.removeEventListener('click', closeOnOutsideClick)
    } else {
      body.classList.add('mobile-menu-is-visible');
      mobileMenu.setAttribute('aria-expanded', 'true');
      document.addEventListener('click', closeOnOutsideClick)
    }
  }

  function destroyMobileMenu() {
    mobileMenuActive = false;

    // Hide toggle 
    if (!options.always_show) {
      mobileToggle.style.display = 'none';
    }

    onClickOrTap(mobileToggle, toggleVisibility, 'remove')

    body.classList.remove('has-mobile-menu')
    body.classList.remove('mobile-menu-is-visible')

    mainNavParent.append(mainNav)

    mobileMenu.remove();

    remove_collapsible_submenus();
  }

  function getAllSiblings(element, parent) {
    const children = [...parent.children];
    return children.filter(child => child !== element);
  }

  function toggleSubMenu(e) {
    let el = e.target
    let subMenu = el.parentNode.querySelector('.sub-menu')
    if (subMenu) {
      e.preventDefault()
      if (el.getAttribute('href') !== '#') {
        el.setAttribute('href', '#');
      }
      el.parentNode.classList.toggle('is-open')
      subMenu.classList.toggle('is-visible')
    }
  }

  // This will allow tapping to open nested menus -- Only way for mobile I could think of
  function setup_collapsible_submenus() {
    mainNav.querySelectorAll('.menu-item-has-children > a').forEach(el => {
      originalHrefs.push({
        el,
        url: el.getAttribute('href'),
      })
    })
    onClickOrTap(mainNav, toggleSubMenu)
  }

  // Reset collapsible states back to normal (so regular clicks work with hover state)
  function remove_collapsible_submenus() {
    // Remove any items that might still be open
    let isVisible = document.querySelectorAll('.is-visible');
    isVisible.forEach(el => {
      el.classList.remove('is-visible')
    })

    // Set links back to what they were
    originalHrefs.forEach(originalHref => {
      originalHref.el.setAttribute('href', originalHref.url)
    })

    // Clean up event listener
    onClickOrTap(mainNav, toggleSubMenu, 'remove')
    // mainNav.removeEventListener('click', toggleSubMenu, false)
  }

  // Toggle Focus on every link desktop and mobile
  (function () {
    // Get all the link elements within the menu.
    let links = mainNav.getElementsByTagName('a');

    if (!links || !links.length) { return }

    // Each time a menu link is focused or blurred, toggle focus.
    for (let i = 0, len = links.length; i < len; i++) {
      links[i].addEventListener('focus', toggleFocus, true);
      links[i].addEventListener('blur', toggleFocus, true);
    }

    /**
     * Sets or removes .focus class on an element.
     */
    function toggleFocus() {
      var self = this;

      // Move up through the ancestors of the current link until we hit .main-menu-nav.
      while (-1 === self.className.indexOf('main-menu-nav')) {
        // On li elements toggle the class .focus.
        if ('li' === self.tagName.toLowerCase()) {
          if (-1 !== self.className.indexOf('focus')) {
            self.className = self.className.replace(' focus', '');
          } else {
            self.className += ' focus';
          }
        }

        self = self.parentElement;
      }
    }


    /**
     * Toggles `focus` class to allow submenu access on tablets.
     */
    (function (container) {
      var touchStartFn,
        i,
        parentLink = container.querySelectorAll('.menu-item-has-children > a, .page_item_has_children > a');

      if ('ontouchstart' in window && parentLink.length) {
        touchStartFn = function (e) {
          var menuItem = this.parentNode,
            i;

          if (!menuItem.classList.contains('focus')) {
            e.preventDefault();
            for (i = 0; i < menuItem.parentNode.children.length; ++i) {
              if (menuItem === menuItem.parentNode.children[i]) {
                continue;
              }
              menuItem.parentNode.children[i].classList.remove('focus');
            }
            menuItem.classList.add('focus');
          } else {
            menuItem.classList.remove('focus');
          }
        };

        for (i = 0; i < parentLink.length; ++i) {
          parentLink[i].addEventListener('touchstart', touchStartFn, false);
        }
      }
    })(mainNav);
  })()

}
