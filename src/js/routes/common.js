import stickyBody from '../components/stickyBody'
import mainMenu from '../components/mainMenu'
import loadMoreVanilla from '../components/loadMoreVanilla'
import postFilter from '../components/postFilter'
import { testiSlider } from '../components/testimonialSlider'
import { projectSlider } from '../components/projectSlider'
import { enableFadeIn, enableStaggerIn } from '../globalAnimations';


export default {
  init() {

    enableFadeIn();
    enableStaggerIn();

    // Main Menu
    mainMenu({
      always_show: true
    });

    stickyBody('#main-header', 20);
  },
  finalize() {

    loadMoreVanilla();
    postFilter();

    // Add correct padding to page based on header height when body is sticky
    (function () {
      let page = document.getElementById('page'),
        mainHeader = document.getElementById('main-header');

      window.addEventListener('body-stickied', function () {
        page.style.paddingTop = `${mainHeader.clientHeight}px`
      })

      window.addEventListener('body-unstickied', function () {
        page.style.paddingTop = 0
      })
    })()

    testiSlider();
    projectSlider();
  },
};
