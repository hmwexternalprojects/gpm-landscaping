<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">

  
  <?php wp_head(); ?>

  <script>
  window.MSInputMethodContext && document.documentMode && document.write(
    '<script src="https://cdn.jsdelivr.net/gh/nuxodin/ie11CustomProperties@4.0.1/ie11CustomProperties.min.js"><\x2fscript>'
  );
  </script>
</head>

<?php 
	$builderClass = '';
	if (function_exists('get_field')) {
		if (get_field('page_builder_enabled', get_the_ID())) {
			$builderClass = 'page-builder-enabled';
		}
	}
?>

<body <?php body_class([$builderClass, 'no-js']); ?>>
<?php wp_body_open(); ?>
  <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hmw' ); ?></a>
  <div id="page" class="site">
    <?php if (get_field('enable_top_header', 'option')) : ?>
    <div id="top-header">
      <div class="container">
        <div id="secondary-menu" class="nav-menu--horizontal">
          <?php
						echo wp_nav_menu( array( 'theme_location' => 'top-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'top-header__menu nav-menu--horizontal', 'menu_id' => 'top-menu', 'echo' => false ) );
					?>
        </div> <!-- #secondary-menu -->
        <?php if (get_field('enable_top_header_phone', 'option')) : ?>
        <div class="header-phone-wrapper flex items-center">
          <i class="fas text-link fa-mobile-alt pr-1"></i>
          <?php echo do_shortcode( '[company-phone]' ); ?>
        </div>
        <?php endif; ?>
      </div> <!-- .comtainer -->
    </div> <!-- #top-header -->
    <?php endif; ?>

    <?php 
    if (get_field('main_header_background_colour', 'option')) : 
      $header_colour = get_field('main_header_background_colour', 'option');
    endif;
  ?>
    <header id="main-header"
      <?php echo (isset($header_colour) && $header_colour !== 'None') ? "class='bg-{$header_colour}'" : null;  ?>>
      <div class="container clearfix">
        <div class="logo_container">
          <?php echo do_shortcode( '[logo]' ); ?>
        </div>      
        
        <div class="header-actions">
          <div class="header-action header-action__phone flex">
            <span class="d-none lg:d-block pr-2 header-font">South Coast NSW & Melbourne</span> <?php echo do_shortcode( '[company-phone text="Get in touch" icon="fas fa-phone"]' ); ?>
          </div>
        </div>

        <div class="mobile-menu">
          <div id="main-nav">
            <nav id="top-menu-nav" class="lg:flex items-center">
              <?php
                  echo wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '', 'fallback_cb' => '', 'menu_class' => 'main-menu-nav', 'menu_id' => 'main-menu', 'echo' => false ) );
                ?>
              <?php 
                if (function_exists('hmw_woocommerce_header_cart')) : 
                  hmw_woocommerce_header_cart(); 
                endif; 
              ?>
            </nav>
          </div> <!-- #main-nav -->
        </div>

        <button class="menu-toggle" id="menu-toggle" aria-expanded="false"><span class="screen-reader-text">Menu</span>
        <svg class="icon icon-menu-toggle" aria-hidden="true" version="1.1" xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 100 100">
          <g class="svg-menu-toggle">
            <path class="line line-1" d="M5 10h90v10H5z" />
            <path class="line line-2" d="M5 43h90v10H5z" />
            <path class="line line-3" d="M5 76h90v10H5z" />
          </g>
        </svg>
      </button>

      </div> <!-- .container -->
    </header> <!-- #main-header -->
